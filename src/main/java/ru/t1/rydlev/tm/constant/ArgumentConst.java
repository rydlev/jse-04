package ru.t1.rydlev.tm.constant;

public final class ArgumentConst {

    public static final String INFO = "-i";

    public static final String VERSION = "-v";

    public static final String HELP = "-h";

}
